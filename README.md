# ioquake3.arm


## 1.) Retropie 4.5.1 



ioquake3.arm  for   cmretropie-4.5.1-rpi2_rpi3.img.gz


   
   wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/cmretropie/-/raw/main/cmretropie-4.5.1-rpi2_rpi3.img.gz" 
   

## 2.) Quake 3 ready for PI

 
https://gitlab.com/openbsd98324/ioquake3.arm/-/raw/main/releases/v2/ioquake3.arm-retropie-retropie-4.5.1-rpi2_rpi3-default-v2.tar.gz

## 3.) Create the directory 

Have the following

 /opt/retropie/ports/quake3/ioquake3.arm  



## 4.) Base

cd ; mkdir .q3a

and add your base directory.


## 5.) Run the game  

Without X11 running, just FB will do the rest: 

/opt/retropie/ports/quake3/ioquake3.arm 



```
opt/retropie/ports/quake3/
opt/retropie/ports/quake3/baseq3
opt/retropie/ports/quake3/ioq3ded.arm
opt/retropie/ports/quake3/ioquake3.arm
home/pi/.q3a/baseq3/
home/pi/.q3a/baseq3/pak4.pk3
home/pi/.q3a/baseq3/pak2.pk3
home/pi/.q3a/baseq3/pak1.pk3
home/pi/.q3a/baseq3/pak6.pk3
home/pi/.q3a/baseq3/pak0.pk3
home/pi/.q3a/baseq3/pak8.pk3
home/pi/.q3a/baseq3/pak7.pk3
home/pi/.q3a/baseq3/pak5.pk3
home/pi/.q3a/baseq3/pak3.pk3
```

![](media/quake-iii-running-on-pi.png)


